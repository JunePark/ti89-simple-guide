# 함수 정의

ref) https://youtu.be/4jmlggDtyAQ

* STO> 키를 이용하여 함수 정의

STO> 키를 선택해서 함수 정의. 함수 사용은 ()를 이용.

![basic02](./images/basic02-0000.png)

f(x), g(x) 를 정의

![basic02](./images/basic02-0001.png)

> NOTE. 그래프에서는 g(a) 라고 하면 안되고 g(x) 라고 독립변수를 x로 명시해야함.

![basic02](./images/basic02-0002.png)

![basic02](./images/basic02-0003.png)


* Define 을 이용하여 함수 정의

F4 -> 1 을 선택하여 Define 선택

![basic02](./images/basic02-0004.png)

> Define i(x)=함수 로 정의

![basic02](./images/basic02-0005.png)

![basic02](./images/basic02-0006.png)

> 마찬가지 이유로 i(x) 로 입력


* 함수가 아닌 변수로 지정한 경우 사용법

![basic02](./images/basic02-0007.png)

> NOTE. r|x=2 와 같은 형식으로 사용. 이렇게 정의하면 그래프에서는 사용하지 못함.


___
.END OF BASIC
