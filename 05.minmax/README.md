# 함수의 최소 최대

* 최소

ref) https://youtu.be/IvG76_4fOPM

F3 -> 6 을 선택하고 함수를 입력한 다음 , X 차례로 입력

![05.minmax](./images/minmax-0000.png)

![05.minmax](./images/minmax-0001.png)

> NOTE. 그래프로 최소값 확인

* 최대

ref) https://youtu.be/IvG76_4fOPM

F3 -> 7 을 선택하고 함수를 입력한 다음 , X 차례로 입력

![05.minmax](./images/minmax-0002.png)

![05.minmax](./images/minmax-0003.png)

> NOTE. 그래프로 최대값 확인

___
.END OF MINMAX
