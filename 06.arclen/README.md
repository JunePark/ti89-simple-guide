# 함수의 최소 최대

* 최소

ref) https://youtu.be/firU502LZ88

F3 -> 8 을 선택하고 함수를 입력한 다음 , X, 시작값, 마지막값을 차례로 입력

![06.arclen](./images/arclen-0000.png)



* 검증
 
그래프 화면에서 F5 -> B를 선택한다음 1st Point(시작점), 2nd Point(마지막점)을 차례대로 입력

![06.arclen](./images/arclen-0001.png)

![06.arclen](./images/arclen-0002.png)

___
.END OF ARCLEN
