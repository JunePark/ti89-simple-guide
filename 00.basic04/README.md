# 연립방정식 해 구하기

ref) https://youtu.be/LqrlyLiKcEg

* 연립 방정식 입력

solve(y=x and y=.5*x+1, {x,y})

> NOTE. and는 MATH 메뉴를 이용해서 2ND + 5 + 8 + 8 키를 차례로 입력

> NOTE. { 는 2ND + ( 키 , } 는 2ND + ) 키
 

* 검증
 
![basic04](./images/basic04-0000.png)

![basic04](./images/basic04-0001.png)

![basic04](./images/basic04-0002.png)

![basic04](./images/basic04-0003.png)

![basic04](./images/basic04-0004.png)

![basic04](./images/basic04-0005.png)

> NOTE. F3 키(Trace)로 cross 부근의 값을 유추할수 있음.

___
.END OF BASIC
