# 쌍곡선 함수 사용법

ref) https://youtu.be/g_HmD10pS30

* 쌍곡선 함수 선택

2ND키 를 누르고 5를 선택하여 나오는 MATH 메뉴에서 C를 선택

![11.hyperbolic](./images/hyperbolic-0000.png)

* 미분

![11.hyperbolic](./images/hyperbolic-0001.png)

* 부정적분

![11.hyperbolic](./images/hyperbolic-0002.png)

* 정적분

![11.hyperbolic](./images/hyperbolic-0003.png)

* 정적분 근사치

![11.hyperbolic](./images/hyperbolic-0004.png)

> NOTE. 녹색키를 누르고 ENTER키를 선택하여 물결무늬 키로 계산하면 근사치 값으로 결과가 나옴.


___
.END OF HYPERBOLIC
