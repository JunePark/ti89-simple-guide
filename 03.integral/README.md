# 적분

* 부정적본

ref) https://youtu.be/yp-SojGceEg

F3 -> 2 을 선택하고 함수를 입력한 다음 , X 를 입력

![integral](./images/integral-0000.png)


* 정적분

ref) https://youtu.be/yp-SojGceEg

F3 -> 2 을 선택하고 함수를 입력한 다음 , X, 하한값, 상한값을 차례로 입력

![integral](./images/integral-0001.png)

___
.END OF INTEGRAL
