# 각도 변환

ref) https://youtu.be/JB3sreS6vNY

* 각도 변환

MATH 메뉴의 2번 (Angle)을 이용

> NOTE. MODE에서 설정은 RAD 으로 고정하고 필요할때만 변환해서 사용하자!
 
![basic06](./images/basic06-0000.png)

![basic06](./images/basic06-0001.png)

![basic06](./images/basic06-0002.png)


___
.END OF BASIC
