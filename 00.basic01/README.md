# 변수값 이용

* 변수값 저장 하고 사용

ref) https://youtu.be/Q8Kvn3u_n90

ON 키 위에 있는 STO> 키를 선택

![basic01](./images/basic01-0000.png)


* 변수값 리스트 보기

2ND 키 를 누르고 VAR-LINK(-키) 선택

![basic01](./images/basic01-0001.png)

___
.END OF BASIC
