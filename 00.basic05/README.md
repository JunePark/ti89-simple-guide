# 식

ref) https://youtu.be/Unm0Onh_-GI

* common denominator

F2 -> 6
 
![basic05](./images/basic05-0000.png)

![basic05](./images/basic05-0001.png)

* expand
 
F2 -> 3

![basic05](./images/basic05-0002.png)

![basic05](./images/basic05-0003.png)


* proper fraction

ref) https://youtu.be/W7hPRFQK2GA

F2 -> 7

![basic05](./images/basic05-0004.png)

![basic05](./images/basic05-0005.png)


* factoring

ref) https://youtu.be/N2betiH4K4A

F2 -> 2

![basic05](./images/basic05-0006.png)

![basic05](./images/basic05-0007.png)

![basic05](./images/basic05-0008.png)

> NOTE. cFactor로 할경우 , X 입력이 필요함.


___
.END OF BASIC
