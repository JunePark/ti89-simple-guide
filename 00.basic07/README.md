# 삼각함수 전개

ref) https://youtu.be/q8jJfV22Zr4

* 전개

F2 -> 9 (Trig) 이용

![basic07](./images/basic07-0000.png)

![basic07](./images/basic07-0001.png)

* Collect

![basic07](./images/basic07-0002.png)


___
.END OF BASIC
