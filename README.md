# TI89 simple guide

TI89 계산기를 그림으로 설명하는 설명서

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## 설명 링크

* Basic (feat. TI-89 Calc Tutor Vol1)
  - [00. log 입력](00.basic00/README.md)
  - [01. 변수값 이용](00.basic01/README.md)
  - [02. 함수 정의](00.basic02/README.md)
  - [03. 방정식 풀이](00.basic03/README.md)
  - [04. 연립방정식 풀이](00.basic04/README.md)
  - [05. 다항식 전개](00.basic05/README.md)
  - [06. 각도 변환](00.basic06/README.md)
  - [07. 삼각함수 전개](00.basic07/README.md)
  - [08. 복소수](00.basic08/README.md)
 

* F3 Calc 메뉴 (feat. TI-89 Calc Tutor Vol2)
  - [01. 극한값](01.limit/README.md)
  - [02. 미분](02.differential/README.md)
  - [03. 적분](03.integral/README.md)
  - [04. 합과곱](04.sumandproduct/README.md)
  - [05. 합수의 최소 최대](05.minmax/README.md)
  - [06. 합수의 곡선길이](06.arclen/README.md)
  - 테일러 급수
  - 

* MATH 메뉴
  - [11. 쌍곡선 함수](06.hyperbolic/README.md)
  - [12.각도 변환1]
  - [13.각도 변환2]
  - [14.벡터]
  - [15.matrix]
  - [16.matrix계산]
  - [16.방정식계산 feat. matrix]

___
.END OF README
