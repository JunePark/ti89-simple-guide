# 합과곱

* 합

ref) https://youtu.be/82vzGFhKkaY

F3 -> 4 을 선택하고 함수를 입력한 다음 , X, 하한값, 상한값을 차례로 입력

![04.sumandproduct](./images/sumandproduct-0000.png)

> NOTE. 무한대는 녹색버튼을 누르고 CATALOG키를 선택

* 곱

ref) https://youtu.be/82vzGFhKkaY

F3 -> 5 을 선택하고 함수를 입력한 다음 , X, 하한값, 상한값을 차례로 입력

![04.sumandproduct](./images/sumandproduct-0001.png)

___
.END OF SUMANDPRODUCT
