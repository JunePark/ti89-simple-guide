# 방정식 해 구하기

ref) https://youtu.be/LcWXHiHd02k

* 항등식 해

F2 -> 1 을 선택해서 solve 기능 사용하기.

![basic03](./images/basic03-0000.png)

![basic03](./images/basic03-0001.png)


* 부등식 해

![basic03](./images/basic03-0002.png)

![basic03](./images/basic03-0003.png)

> NOTE. MATH(2ND + 5키) -> 8 을 이용하여 관계연산자 선택할 수 있다. (참고로 >, < 키는 별도로 있음.)


* Zero 값 찾기

![basic03](./images/basic03-0004.png)

![basic03](./images/basic03-0005.png)

> NOTE. 보다시피 solve() 를 이용하여도 같은 결과를 얻을 수 있기 때문에 자주 사용하지 않음.


* 복소수 해

MATH(2ND + 5키) -> A -> 1을 이용하여 복소수 해를 찾을수 있다.

![basic03](./images/basic03-0006.png)

![basic03](./images/basic03-0007.png)


* 여러개의 변수값에 대한 solve 기능

TI-89의 막강한 기능

![basic03](./images/basic03-0008.png)

![basic03](./images/basic03-0009.png)


* nSolve() 이용

ref) https://youtu.be/JE9h8wcYeJo

한개의 해를 찾을 때 이용. (자주 사용하지는 않는 기능일듯.)

![basic03](./images/basic03-0010.png)

![basic03](./images/basic03-0011.png)


* 삼각함수의 해

ref) https://youtu.be/kD0JvKDK5yo

![basic03](./images/basic03-0012.png)

> NOTE. @n1 은 모든 정수를 의미 (... -2, -1, 0, 1, 2, ...)

___
.END OF BASIC
