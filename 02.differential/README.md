# 미분

* 도함수 (Derivatives)

ref) https://youtu.be/j3-GKsSHvh4

F3 -> 1 을 선택하고 함수를 입력한 다음 , X 를 입력

![differential](./images/differential-0000.png)

![differential](./images/differential-0001.png)


* 편도함수 (Partial Derivatives)

ref) https://youtu.be/j3-GKsSHvh4

F3 -> 1 을 선택하고 함수를 입력한 다음 , X 를 입력

![differential](./images/differential-0002.png)

> NOTE. 도함수와 동일


* 음함수(Implicit Differentiation)

ref) https://youtu.be/VCw2UHM3D4Q

F3 -> D 을 선택하고 함수를 입력한 다음 , X, Y 를 차례대로 입력(여기서 X는 독립변수, Y는 종속변수)

![differential](./images/differential-0003.png)

![differential](./images/differential-0004.png)

___
.END OF DIFFERENTIAL
