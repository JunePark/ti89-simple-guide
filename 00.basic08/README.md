# 복소수

ref) https://youtu.be/mNKBqC620VI

* 각도 변환

MATH 메뉴의 5번 (Complex)을 이용

> NOTE. MODE에서 설정은 RAD 으로 고정하고 필요할때만 변환해서 사용하자!
 
![basic08](./images/basic08-0000.png)

![basic08](./images/basic08-0001.png)

![basic08](./images/basic08-0002.png)

![basic08](./images/basic08-0003.png)


___
.END OF BASIC
